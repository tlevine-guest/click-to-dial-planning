Date:    Wed, 11 Apr 2018 22:53:36 +0200
From:    Daniel Pocock
To:      Thomas Levine

 At first glance, one student can actually make a functional solution,
that is completely true.  However, there are many related topics and
permutations, I only mentioned some of them, here are some more details
and links:

- Marina Zhurakhinskaya finally replied from GNOME on 8 November, she
mentioned that a GNOME-specific solution might follow a different
architecture: "would be most appropriate to use the Flatpak Portal
technology - https://github.com/flatpak/flatpak/wiki/Portals - to
implement a mechanism to call a phone number listed in one application
with an application of user's choice. Allan Day (allanpday@gmail.com)
and Matthias Clasen (matthias.clasen@gmail.com) are the best people to
coordinate on this further, including on designing an interface."

- maybe there are other strategies that might be relevant in other
desktop environments.

- I wonder if the code can be divided into a library (for all desktops)
and then some wrappers and packages for desktop-specific integrations.
Or maybe the Flatpak Portal thing would just be an independent project.

- sipdial is here, it is potentially going to handle events from the
click-to-dial window, it works but it could be improved (C++):
https://github.com/resiprocate/resiprocate/tree/master/apps/sipdial

- Jaminy was an intern in 2016, she started a script for scanning IMAP
folders, checking for phone numbers in each message, normalizing with
libphonenumber and comparing to the address book entry for the message
senders, helping people merge any numbers it found into their address
book.  It wasn't fully completed and a student could continue it.

- it would be interesting to have a mechanism to transport a clicked
phone number from the PC to a nearby mobile phone.  That could also be a
sub-project for a student to develop the necessary code on both PC and
phone.

- there is the Telify project (browser extension), to make any number on
a web page clickable:
https://wiki.debian.org/SummerOfCode2018/Projects#SummerOfCode2018.2FProjectProposals.2FCreateASuccessorToTelifyUsingWebExtensions.Create_a_successor_to_the_Telify_plugin_using_WebExtensions

- while it is a bit of a stretch from click-to-dial, I am also keen to
see a student experiment with some code for Push-to-talk (PTT) in GNU
Radio: to take joystick button events and use them to go from RX to TX mode

Regards,

Daniel


===============
Date:    Thu, 12 Apr 2018 19:48:41 +0200
From:    Daniel Pocock
To:      Thomas Levine

Thomas wrote:
> Daniel wrote:
>> On the question of having 3 students on this topic: Alex wrote a note in
>> the spreadsheet that he felt it is too simple for more than one student.
>>  At first glance, one student can actually make a functional solution,
>> that is completely true.  However, there are many related topics and
>> permutations, I only mentioned some of them, here are some more details
>> and links:
> 
> I agree. Expecting only one student at first, I put on the wiki
> a proposal for a minimally functional project, but my impression was
> always that that project would be useful only because it would
> facilitate the addition of the more features in the future. Here
> are some ideas, organized by my three parts.
> 
> 1. In order for the program to work outside of GNOME, we will need to
>    configure the handler for many different programs. Doing this
>    properly could involve learning the configuration for many softwares
>    and submitting patches to many different packages. More likely
>    for the summer is is just documentation, but such documentation might
>    make for a useful resourse far beyond tel URL handlers.
> 2. The pop-up window can similarly be developed for wide use. Some
>    considerations that might take time are tiling window managers and
>    people who can't see. And perhaps there could be a totally separate
>    pop-up window program for terminal interfaces.
> 3. You have recounted many things one could do with a phone number.
>    Sanjay and Vishal have both proposed even more, with clear
>    implementation plans.
> 


Here are some more:

Context-awareness, especially when running on laptops:
- does the user have headset connected?  If yes, default action is to
dial through softphone like Jitsi
- is the user on their office LAN?  Did they configure PBX integration?
If yes, default action is to dial through the phone on their desk
- is the user's mobile phone contactable through Bluetooth or a push
service?  If yes, default action is to send the dialed number to the mobile.
- if all else fails, fall back to softphone

To support all of that, various libraries may need to be developed:
- for using Ring's OpenDHT network to find a mobile device for push
notifications (instead of using Google's push mechanism)
- a server-side daemon that can run on each type of PBX, e.g. Asterisk
and FreeSWITCH

A mechanism for sending click-to-dial back to the browser for a WebRTC
client like freephonebox.net to dial the number

ENUM library to support click-to-dial, continue work on Omnidial or port
it to Python:
http://omnidial.org

Integrating the ENUM library into the click-to-dial window, so it can
discover things about the phone number.

Extend the TBDialOut plugin for Thunderbird so that when a user
right-clicks an email address (e.g. message sender), a new menu option
appears for them to call that person.

Same as above but for K-9 mail app, I already started it:
https://github.com/k9mail/k-9/issues/909
https://github.com/k9mail/k-9/pull/866
https://groups.google.com/forum/#!msg/k-9-dev/ZYdXbklpNXg/2pwErn1pBwAJ

Another student started working on a generic mechanism for people to
choose from a list of SIP providers and register an account quickly,
that could be helpful.

Regards,

Daniel
